# Printful Quiz

## Development Server
1) Make sure to install npm packages `npm install`
2) Run dev server `npm run dev`

## Architecture

1) `/api` directory contains required endpoint requests
2) `/store` using vuex to store data from API
3) `/components` reusable components go here
4) `/views` where all the main "pages" are.
