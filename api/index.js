import axios from 'axios';

const BASE_URL = 'https://printful.com/test-quiz.php?action=';

export const fetchQuizzes = () => {
    return axios.get(`${BASE_URL}quizzes`);
}

export const fetchQuestions = (quizId) => {
    return axios.get(`${BASE_URL}questions&quizId=${quizId}`);
}

export const fetchAnswers = (quizId, questionId) => {
    return axios.get(`${BASE_URL}answers&quizId=${quizId}&questionId=${questionId}`);
}

export const fetchResult = (quizId, answers) => {
    let answersQuery = '';

    answers.forEach(answer => {
        answersQuery += `&answers[]=${answer.id}`;
    })

    return axios.get(`${BASE_URL}submit&quizId=${quizId}${answersQuery}`);
}

export default {
    fetchQuizzes,
    fetchQuestions,
    fetchAnswers,
    fetchResult
}