import Vue from 'vue';

import Input from '@/components/Input';
import Select from '@/components/Select';
import Button from '@/components/Button';

Vue.component('Input', Input);
Vue.component('Select', Select);
Vue.component('Button', Button);