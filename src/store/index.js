import Vue from 'vue';
import Vuex from 'vuex';
import api from '/api';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        quizzes: [],
        user: {
            name: '',
            quizId: null
        },
        questions: [],
        answers: [],
        selectedAnswers: [],
        result: null
    },
    getters: {
        totalSteps: state => state.questions.length
    },
    mutations: {
        setQuizzes(state, quizzes) {
            state.quizzes = quizzes;
        },
        setUser(state, {name, quizId}) {
            state.user.name = name;
            state.user.quizId = quizId;
        },
        setQuestions(state, questions) {
            state.questions = questions;
        },
        setAnswers(state, answers) {
            state.answers = answers;
        },
        addSelectedAnswer(state, answer) {
            state.selectedAnswers.push(answer);
        },
        setResult(state, result) {
            state.result = result;
        }
    },
    actions: {
        async fetchQuizzes({commit}) {
            const res = await api.fetchQuizzes();
            commit('setQuizzes', res.data);
        },
        async fetchQuestions({state, commit}) {
            const questions = (await api.fetchQuestions(state.user.quizId)).data;
            commit('setQuestions', questions);
        },
        async fetchAnswers({state, commit}) {
            const allAnswers = [];

            await Promise.all(state.questions.map(async (question) => {
                const answers = (await api.fetchAnswers(state.user.quizId, question.id)).data;
                allAnswers.push({questionId: question.id, answers});
            }))

            commit('setAnswers', allAnswers);
        },
        async fetchResult({state, commit}) {
            const result = (await api.fetchResult(state.user.quizId, state.selectedAnswers)).data;
            commit('setResult', result);
        }
    }
})
