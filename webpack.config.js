const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const HtmlWebPackPlugin = require( 'html-webpack-plugin' );

module.exports = {
    mode: "development",
    entry: {
        main: "./src/main.js",
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    {
                        loader: 'css-loader',
                        options: { esModule: false }
                    },
                    "sass-loader",
                ],
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    outputPath: 'static/img',
                    esModule: false
                },
            }
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebPackPlugin({
            template: './src/index.html',
        }),
    ],
    resolve: {
        extensions: ['.js', '.json', '.vue'],
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
    },
    devServer: {
        contentBase: path.join(__dirname, '/dist'),
        port: 8083,
    },
};
